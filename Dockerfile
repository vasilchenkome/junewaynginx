FROM debian:9 as build

RUN apt update && apt install -y wget gcc make libpcre3 libpcre3-dev zlib1g zlib1g-dev
RUN cd /usr/local/src && wget http://nginx.org/download/nginx-1.19.3.tar.gz && tar xvfz nginx-1.19.3.tar.gz
RUN cd /usr/local/src/nginx-1.19.3 && ./configure && make -j2 && make install

FROM debian:9

COPY --from=build /usr/local/nginx /usr/local/nginx
WORKDIR /usr/local/nginx/sbin
EXPOSE 80

CMD ["./nginx","-g","daemon off;"]
