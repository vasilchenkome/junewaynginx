Собираем образ:

```sh
docker build -t junewaynginx:v1 .
```


Запускаем контейнер с нашим конфигом:
```sh
docker run -d -it --name junewaynginx -v $(pwd)/conf/nginx.conf:/usr/local/nginx/conf/nginx.conf -p 8000:80 junewaynginx:v1
```